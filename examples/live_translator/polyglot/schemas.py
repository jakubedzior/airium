import time

from pydantic import BaseModel

import airium


class Context:
    last_translation: str = ""


class TranslationRequest(BaseModel):
    language: str
    content: str = ""

    def __str__(self):
        return f"\nDocument.language: {self.language} len:{len(self.content)} chars\n"


def human_readable_time(seconds: float) -> str:
    assert seconds >= 0.0
    unit = "s"
    next_units = ["ms", "us", "ns"]
    while seconds and seconds < 1.0 and next_units:
        seconds *= 1000
        unit = next_units.pop(0)
    return f"{seconds:.02f} {unit}"


class TranslationResponse(TranslationRequest):
    time_consumed: str = ""
    is_error: bool = False

    @classmethod
    def from_translation_request(cls, translation_req) -> "TranslationResponse":
        started = time.time()
        result = cls.translation_impl(translation_req)
        result.time_consumed = human_readable_time(time.time() - started)
        return result

    @classmethod
    def translation_impl(cls, translation_req):
        src_lang = translation_req.language
        dst_lang = "py" if translation_req.language == "html" else "html"

        if src_lang not in ("py", "html"):
            return cls(language=src_lang, content=f"Unknown source language: {src_lang}", is_error=True)
        try:
            if src_lang == "html":
                Context.last_translation = translation_req.content
                py_code = airium.from_html_to_airium(translation_req.content, include_module_head=False)
                return TranslationResponse(language=dst_lang, content=py_code)

            else:
                html_code = from_airium_to_html(translation_req.content)
                Context.last_translation = html_code
                return TranslationResponse(language=dst_lang, content=html_code)

        except Exception as e:
            error_text = f"Failed to translate the document\nGot {type(e).__name__}: {e}"
            return TranslationResponse(language=dst_lang, content=error_text, is_error=True)


def from_airium_to_html(content: str) -> (str, str):
    a = airium.Airium()
    exec(content)
    return bytes(a)
