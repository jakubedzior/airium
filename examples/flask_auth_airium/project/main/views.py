import contextlib

from flask import url_for
from flask_login import current_user

from airium import Airium


@contextlib.contextmanager
def base_wrapper():
    a = Airium()
    a("<!DOCTYPE html>")
    with a.html():
        with a.head():
            a.meta(charset="utf-8")
            a.meta(content="IE=edge", **{"http-equiv": "X-UA-Compatible"})
            a.meta(content="width=device-width, initial-scale=1", name="viewport")
            a.title(_t="Flask Auth Example")
            a.link(
                href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css",
                rel="stylesheet",
            )

        with a.body():
            with a.section(klass="hero is-primary is-fullheight"):
                with a.div(klass="hero-head"):
                    navigation_block(a)
                with a.div(klass="hero-body"):
                    # it's equivalent to {% block content %}
                    yield a
                    # after that it's like {% endblock %}


def navigation_block(a):
    with a.nav(klass="navbar"):
        with a.div(klass="container"):
            with a.div(klass="navbar-menu", id="navbarMenuHeroA"):
                with a.div(klass="navbar-end"):

                    a.a(klass="navbar-item", href=url_for("main.index"), _t="Home")

                    if current_user.is_authenticated:
                        a.a(klass="navbar-item", href=url_for("main.profile"), _t="Profile")
                        a.a(klass="navbar-item", href=url_for("auth.logout"), _t="Logout")

                    else:
                        a.a(klass="navbar-item", href=url_for("auth.login"), _t="Login")
                        a.a(klass="navbar-item", href=url_for("auth.signup"), _t="Sign Up")


def index_block(a):
    with a.div(klass="container has-text-centered"):
        a.h1(klass="title", _t="Flask Login Example")
        a.h2(klass="subtitle", _t="Easy authentication and authorization in Flask.")

    developers_info_block(a)


def profile_block(a, name):
    with a.div(klass="container has-text-centered"):
        a.h1(klass="title", _t=f"Welcome, {name}!")

        a(f"You are logged in with <b>{current_user.email}</b> email address.")


def developers_info_block(a):
    with a.div(klass="container has-text-centered"):
        a.h2(klass="title is-primary", _t="Developers' info:")
        with a.div(klass="box"):
            with a.table(klass="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"):
                with a.thead():
                    with a.tr():
                        a.th(_t="attribute")
                        a.th(_t="type")
                        a.th(_t="value")

                with a.tbody():
                    for attribute in dir(current_user):
                        if not attribute.startswith("_"):
                            value = getattr(current_user, attribute)
                            with a.tr():
                                a.td(_t=f"current_user.{attribute}")
                                a.td(_t=type(value).__name__)
                                a.td(_t=repr(value).replace("<", "").replace(">", ""))
