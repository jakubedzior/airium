from flask import get_flashed_messages, url_for


def login_block(a):
    with a.div(klass="column is-4 is-offset-4"):
        a.h3(klass="title", _t="Login")
        with a.div(klass="box"):
            for msg in get_flashed_messages():
                a.div(klass="notification is-danger", _t=msg)

            with a.form(action="/login", method="POST"):
                with a.div(klass="field"):
                    with a.div(klass="control"):
                        a.input(
                            autofocus="",
                            klass="input is-large",
                            name="email",
                            placeholder="Your Email",
                            type="email",
                        )
                with a.div(klass="field"):
                    with a.div(klass="control"):
                        a.input(
                            klass="input is-large",
                            name="password",
                            placeholder="Your Password",
                            type="password",
                        )
                with a.div(klass="field"):
                    with a.label(klass="checkbox"):
                        a.input(type="checkbox", name="remember")
                        a("Remember me")
                a.button(klass="button is-block is-info is-large is-fullwidth", _t="Login")


def signup_block(a):
    with a.div(klass="column is-4 is-offset-4"):
        a.h3(klass="title", _t="Sign Up")
        with a.div(klass="box"):

            for msg in get_flashed_messages():
                with a.div(klass="notification is-danger"):
                    a(msg)
                    a("Go to")
                    a.a(href=url_for("auth.login"), _t="login page")
                    a(".")

            with a.form(action="/signup", method="POST"):
                with a.div(klass="field"):
                    with a.div(klass="control"):
                        a.input(
                            autofocus="",
                            klass="input is-large",
                            name="email",
                            placeholder="Email",
                            type="email",
                        )
                with a.div(klass="field"):
                    with a.div(klass="control"):
                        a.input(
                            autofocus="",
                            klass="input is-large",
                            name="name",
                            placeholder="Name",
                            type="text",
                        )
                with a.div(klass="field"):
                    with a.div(klass="control"):
                        a.input(
                            klass="input is-large",
                            name="password",
                            placeholder="Password",
                            type="password",
                        )

                a.button(klass="button is-block is-info is-large is-fullwidth", _t="Sign Up")
